/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.29-0ubuntu0.18.04.1 : Database - crypto_trading
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`crypto_trading` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `crypto_trading`;

/*Table structure for table `symbol_huobi` */

DROP TABLE IF EXISTS `symbol_huobi`;

CREATE TABLE `symbol_huobi` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(45) NOT NULL,
  `base_c` varchar(45) NOT NULL,
  `counter_c` varchar(45) NOT NULL,
  `enable` tinyint(4) DEFAULT NULL,
  `price` decimal(16,8) DEFAULT NULL,
  `time_range` int(11) DEFAULT NULL,
  `price_diff` decimal(5,2) DEFAULT NULL,
  `price_diff_value` decimal(16,8) DEFAULT NULL,
  `spread_change` decimal(5,2) DEFAULT NULL,
  `spread` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `spread_hedge` decimal(11,5) NOT NULL DEFAULT '0.20000',
  `hedge_market` tinyint(4) NOT NULL DEFAULT '0',
  `max_qty_bid` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `max_qty_ask` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `account` varchar(45) DEFAULT NULL,
  `total_order` int(11) DEFAULT '10',
  PRIMARY KEY (`id`),
  UNIQUE KEY `symbol_UNIQUE` (`symbol`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

/*Data for the table `symbol_huobi` */

insert  into `symbol_huobi`(`id`,`symbol`,`base_c`,`counter_c`,`enable`,`price`,`time_range`,`price_diff`,`price_diff_value`,`spread_change`,`spread`,`spread_hedge`,`hedge_market`,`max_qty_bid`,`max_qty_ask`,`account`,`total_order`) values (1,'TRXBTC','TRX','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'39580.93714','15101.25388','huobi_merry_snap',10),(3,'TRXUSDT','TRX','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'39580.93714','15101.25388','huobi_merry_snap',10),(4,'DASHUSDT','DASH','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'4.07121','7.06369','huobi_merry_snap',10),(18,'DASHBTC','DASH','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.86761','7.23937','huobi_merry_snap',10),(19,'BATUSDT','BAT','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.00000','10.97800','huobi_merry_snap',10),(20,'BATBTC','BAT','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.00000','10.97800','huobi_merry_snap',10),(22,'XMRBTC','XMR','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'43.77831','10.32406','huobi_merry_snap',10),(23,'ADABTC','ADA','BTC',0,'0.05903000',0,'0.05','0.06780810','0.09','0.10000','0.08000',0,'2813.48120','411.14950','huobi_merry_snap',10),(24,'ADAUSDT','ADA','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'707.59120','2555.39727','huobi_merry_snap',10),(25,'EOSUSDT','EOS','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'83.59600','50.06641','huobi_merry_snap',10),(26,'NEOUSDT','NEO','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'1.72692','205.56100','huobi_merry_snap',10),(27,'XRPUSDT','XRP','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'23.71730','40.94077','huobi_merry_snap',10),(28,'ZILUSDT','ZIL','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'2373.49000','6384.51858','huobi_merry_snap',10),(29,'ZILBTC','ZIL','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'1426.51000','7169.14060','huobi_merry_snap',10),(30,'ETHUSDT','ETH','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.00965','2.05287','huobi_merry_snap',10),(31,'ETHBTC','ETH','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.00965','2.05287','huobi_merry_snap',10),(32,'ONTBTC','ONT','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.00000','0.00000','huobi_merry_snap',10),(33,'ADAETH','ADA','ETH',0,'0.05911000',0,'0.01','0.15249068','0.09','0.10000','0.08000',0,'2096.20120','14591.96849','huobi_merry_snap',10),(34,'BATETH','BAT','ETH',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.00000','10.97800','huobi_merry_snap',10),(35,'XRPBTC','XRP','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'21.56730','53.75377','huobi_merry_snap',10),(36,'EOSBTC','EOS','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'137.94000','0.00000','huobi_merry_snap',10),(37,'EOSETH','EOS','ETH',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'137.94000','0.00000','huobi_merry_snap',10),(38,'LTCBTC','LTC','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.00677','1.13103','huobi_merry_snap',10),(39,'LTCUSDT','LTC','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'0.00677','1.13103','huobi_merry_snap',10),(40,'QTUMUSDT','QTUM','USDT',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'267.29844','54.53715','huobi_merry_snap',10),(41,'QTUMBTC','QTUM','BTC',0,NULL,NULL,NULL,NULL,NULL,'0.10000','0.08000',0,'267.29844','54.53715','huobi_merry_snap',10);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
