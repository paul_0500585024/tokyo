/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.29-0ubuntu0.18.04.1 : Database - crypto_trading
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`crypto_trading` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `crypto_trading`;

/*Table structure for table `symbol_hitbtc` */

DROP TABLE IF EXISTS `symbol_hitbtc`;

CREATE TABLE `symbol_hitbtc` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(45) NOT NULL,
  `base_c` varchar(45) NOT NULL,
  `counter_c` varchar(45) NOT NULL,
  `enable` tinyint(4) DEFAULT NULL,
  `min_diff_trade` decimal(11,5) DEFAULT '1.00000',
  `min_diff_report` decimal(11,5) DEFAULT '0.10000',
  `price` decimal(16,8) DEFAULT NULL,
  `time_range` int(11) DEFAULT NULL,
  `price_diff` decimal(5,2) DEFAULT NULL,
  `price_diff_value` decimal(16,8) DEFAULT NULL,
  `spread_change` decimal(5,2) DEFAULT NULL,
  `spread` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `spread_hedge` decimal(11,5) NOT NULL DEFAULT '0.20000',
  `max_qty_bid` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `max_qty_ask` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `account` varchar(45) DEFAULT NULL COMMENT 'Kucoin account name ',
  `total_order` int(11) DEFAULT '10',
  `hedge_market` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=241 DEFAULT CHARSET=latin1;

/*Data for the table `symbol_hitbtc` */

insert  into `symbol_hitbtc`(`id`,`symbol`,`base_c`,`counter_c`,`enable`,`min_diff_trade`,`min_diff_report`,`price`,`time_range`,`price_diff`,`price_diff_value`,`spread_change`,`spread`,`spread_hedge`,`max_qty_bid`,`max_qty_ask`,`account`,`total_order`,`hedge_market`) values (1,'ETHUSDT','ETH','USDT',0,'1.00000','1.00000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','0.27431','0.79434','hitbtc_crypto1',10,0),(209,'ADAUSDT','ADA','USDT',0,'1.00000','1.00000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','133.32120','13457.30000','hitbtc_crypto1',10,0),(210,'ADABTC','ADA','BTC',0,'1.00000','1.00000','0.05903000',0,'0.05','0.06780810','0.09','0.11000','0.08000','133.32120','13457.30000','hitbtc_crypto1',10,0),(211,'NEOUSDT','NEO','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','17.28082','2.20300','hitbtc_crypto1',10,0),(212,'NEOBTC','NEO','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','2.46622','60.32000','hitbtc_crypto1',10,0),(213,'NEOETH','NEO','ETH',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','1.72692','1.72200','hitbtc_crypto1',10,0),(216,'TRXUSDT','TRX','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','370.62407','695.79221','hitbtc_crypto1',10,0),(217,'TRXBTC','TRX','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','1367.84407','14449.29221','hitbtc_crypto1',10,0),(218,'ONTUSDT','ONT','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','96.73546','2.44000','hitbtc_crypto1',10,0),(219,'XLMUSDT','XLM','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','15.00000','15.00000','hitbtc_crypto1',10,0),(220,'QTUMUSDT','QTUM','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','2.62084','250.73000','hitbtc_crypto1',10,0),(221,'QTUMBTC','QTUM','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','2.41274','210.26800','hitbtc_crypto1',10,0),(222,'ADAETH','ADA','ETH',0,'1.00000','0.10000','0.05911000',0,'0.01','0.15249068','0.09','0.11000','0.08000','2096.20120','263.70000','hitbtc_crypto1',10,0),(223,'BNBUSDT','BNB','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','5.01776','29.23080','hitbtc_crypto1',10,0),(224,'BNBBTC','BNB','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','71.19366','19.45780','hitbtc_crypto1',10,0),(225,'ONTETH','ONT','ETH',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','96.73546','2.44000','hitbtc_crypto1',10,0),(226,'QTUMETH','QTUM','ETH',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','287.72153','7.21000','hitbtc_crypto1',10,0),(227,'ZILBTC','ZIL','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','2366.80000','5531.70000','hitbtc_crypto1',10,0),(228,'ZILUSDT','ZIL','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','2366.80000','5531.70000','hitbtc_crypto1',10,0),(229,'IOTABTC','IOTA','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','0.00000','10.00000','hitbtc_crypto1',10,0),(233,'EOSUSDT','EOS','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','2.40800','8.81600','hitbtc_crypto1',10,0),(234,'EOSBTC','EOS','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','2.40800','8.81600','hitbtc_crypto1',10,0),(235,'XMRBTC','XMR','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','11.81421','35.19500','hitbtc_crypto1',10,0),(237,'BTCUSDT','BTC','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','0.19908','0.21189','hitbtc_crypto1',10,0),(238,'LTCBTC','LTC','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','0.24504','0.73020','hitbtc_crypto1',10,0),(239,'LTCUSDT','LTC','USDT',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','0.24504','0.73020','hitbtc_crypto1',10,0),(240,'DASHBTC','DASH','BTC',0,'1.00000','0.10000','0.10000000',NULL,NULL,NULL,NULL,'0.11000','0.08000','0.99421','3.24360','hitbtc_crypto1',10,0);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
