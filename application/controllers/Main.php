<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        //load library form validasi
        $this->load->helper('file');
        $this->load->library('form_validation');
    }

    public function index()
    {
        if($this->session->userdata('logged_in') == "true")
        {
            //if registered, go to dashboard
            redirect(base_url()."crypto/hitbtc");

        }else{

            //if not registered

            //set form validation
            $this->form_validation->set_rules('username', 'Username', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');

            //set message form validation
            $this->form_validation->set_message('required', '<div class="alert alert-danger" style="margin-top: 3px">
	                <div class="header"><b><i class="fa fa-exclamation-circle"></i> {field}</b> harus diisi</div></div>');

            //check validasi
            if ($this->form_validation->run() == TRUE) {

                //get data dari FORM
                $username = $this->input->post("username", TRUE);
                $password = md5($this->input->post('password', TRUE));

                //checking data
                $checking = FALSE;
                $this->config->load('logindata');
                $logindata = $this->config->item('logindata');

                foreach($logindata as $each){
                    if($each->username === $username && $each->password === $password){
                        $checking = array(
                            'user_id' => '1',
                            'user_name' => $each->username,
                            'user_pass' => $each->password,
                            'name' => 'crypto_trading_tokyo',
                        );
                    }
                }

                //jika ditemukan, maka create session
                if ($checking != FALSE) {
                    foreach ($checking as $apps) {

                        $session_data = array(
                            'user_id'   => $apps->id_user,
                            'user_name' => $apps->username,
                            'user_pass' => $apps->password,
                            'logged_in' => TRUE
                        );
                        //set session userdata
                        $this->session->set_userdata($session_data);

                        redirect(base_url().'crypto/hitbtc');

                    }
                }else{

                    $data['error'] = '<div class="alert alert-danger" style="margin-top: 3px">
	                	<div class="header"><b><i class="fa fa-exclamation-circle"></i> ERROR</b> username atau password is wrong!</div></div>';
                    $this->load->view('login', $data);
                }

            }else{
/*                $data = '$config[\'logindata\'][2] = array(\'username\' => \'snap2\', \'password\' => \'tokyo2021\');';
                $file_path = APPPATH . "/config/logindata.php";
                if(file_exists($file_path))
                {
                    write_file($file_path, $data, 'a');
                }

                $this->config->load('logindata');
                var_dump($this->config->item('logindata'));*/

                $this->load->view('login');
            }

        }

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url().'main');
    }
}
