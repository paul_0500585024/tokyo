<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kucoin_model extends CI_Model
{
    private $table = 'symbol_kucoin';
    private $columns = array(
        'id','symbol','base_c','enable','spread','max_qty_bid','max_qty_ask','account','spread_hedge','counter_c`'
    );

    private function build_get($params = array())
    {
        $col = implode (", ", $this->columns);
        $this->db->select($col);
        if(isset($params["search"]["value"])){
            $this->db->like('symbol', $params["search"]["value"]);
            $this->db->or_like('base_c', $params["search"]["value"]);
            $this->db->or_like('enable', $params["search"]["value"]);
            $this->db->or_like('spread', $params["search"]["value"]);
            $this->db->or_like('max_qty_bid', $params["search"]["value"]);
            $this->db->or_like('max_qty_ask', $params["search"]["value"]);
            $this->db->or_like('account', $params["search"]["value"]);
            $this->db->or_like('spread_hedge', $params["search"]["value"]);
            $this->db->or_like('counter_c', $params["search"]["value"]);
        }

        if(isset($params["order"])){
            $this->db->order_by($this->columns[$params['order']['0']['column']+1], $params['order']['0']['dir']);
        }else{
            $this->db->order_by('id', 'DESC');
        }
    }

    public function get($params = array())
    {
        $this->build_get($params);
        $data['number_filter_row'] = $this->db->get($this->table)->num_rows();

        $this->build_get($params);
        if($params["length"] != -1)
        {
            $this->db->limit($params['length'], $params['start']);
        }
        $data['result'] = $this->db->get($this->table);

        return $data;
    }

    public function get_all()
    {
        return $this->db->get($this->table);
    }

    public function insert($params = array())
    {
        $retval = FALSE;
        if($this->db->insert($this->table,$params)){
            if($this->db->affected_rows() > 0)
            {
                $retval = TRUE;
            }
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }

        return $retval;
    }

    public function update($params = array())
    {
/*        $data[$params['column_name']] = $params["value"];
        $this->db->update($this->table, $data, array('id' => $params['id']));
        $retval = FALSE;
        if($this->db->affected_rows() > 0)
        {
            $retval = TRUE;
        }
        return $retval;*/

        $retval = FALSE;
        if($this->db->update($this->table, $params, array('id' => $params['id']))){
            $retval = TRUE;
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }
        return $retval;

    }

    public function delete($params = array())
    {
        $retval = FALSE;
        if($this->db->delete($this->table, array('id' => $params['id']))){
            if($this->db->affected_rows() > 0)
            {
                $retval = TRUE;
            }
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }
        return $retval;
    }

    public function enable_all()
    {
        $retval = FALSE;
        $this->db->set('enable','1');
        if($this->db->update($this->table)){
            $retval = TRUE;
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }
        return $retval;
    }

    public function disable_all()
    {
        $retval = FALSE;
        $this->db->set('enable','0');
        if($this->db->update($this->table)){
            $retval = TRUE;
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }
        return $retval;
    }

}