<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crypto_model extends CI_Model
{

    public function __construct()
    {
        $this->load->helper('tokyo');
        $this->table = TABLE_PREFIX.$this->uri->segment(2);
        $this->table_normal = $this->is_table_normal();
        $this->enable_button_exist = $this->is_enable_button_exist();
    }

    public function is_table_normal()
    {
        $retval = false;
        $fields = $this->field_data();
        foreach($fields as $each){
            if($each->name == 'id'){
                $retval = true;
            }
        }
        return $retval;
    }

    public function is_enable_button_exist()
    {
        $retval = false;
        $fields = $this->field_data();
        foreach($fields as $each){
            if($each->name == 'enable'){
                $retval = true;
            }
        }
        return $retval;
    }

    public function field_data(){
        $this->columns = array();
        if (($query = $this->db->query('SHOW COLUMNS FROM '.$this->table)) === FALSE)
        {
            return FALSE;
        }
        $query = $query->result_object();

        $retval = array();
        for ($i = 0, $c = count($query); $i < $c; $i++)
        {
            $retval[$i]			= new stdClass();
            $retval[$i]->name		= $query[$i]->Field;

            sscanf($query[$i]->Type, '%[a-z](%d,%d)',
                $retval[$i]->type,
                $retval[$i]->max_length,
                $retval[$i]->numeric_scale
            );
            $this->columns[$i] = $query[$i]->Field;
            $retval[$i]->default		= $query[$i]->Default;
            $retval[$i]->primary_key	= (int) ($query[$i]->Key === 'PRI');
        }

        return $retval;

    }

    public function check_table(){
        return $this->db->table_exists($this->table);
    }

    public function get_all_table(){
        $tables = $this->db->list_tables();
        $spesifix_tables = array();
        foreach($tables as $each){
            if(startsWith($each,TABLE_PREFIX)){
                $spesifix_tables[] = $each;
            }
        }
        return $spesifix_tables;
    }


    private function build_get($params = array())
    {
        $this->columns_all = $this->field_data(); //to create columns
        $col = implode (", ", $this->columns);
        $this->db->select($col);
        if(isset($params["search"]["value"])){
            $inc = 0;
            $columns = array();
            foreach($this->columns_all as $each){
                if($each->name != 'id'){
                    $columns[$inc] = $each;
                    $inc++;
                }
            }
            foreach($columns as $inc=>$each_col){
                if($inc == 0){
                    $this->db->like($each_col->name, $params["search"]["value"]);
                }else{
                    $this->db->or_like($each_col->name, $params["search"]["value"]);
                }
            }
        }

        if(isset($params["order"])){
            $this->db->order_by($this->columns[$params['order']['0']['column']], $params['order']['0']['dir']);
        }else{
            if($this->table_normal){
                $this->db->order_by('id', 'DESC');
            }
        }
    }


    public function get($params = array())
    {
        $this->build_get($params);
        $data['number_filter_row'] = $this->db->get($this->table)->num_rows();

        $this->build_get($params);
        if($params["length"] != -1)
        {
            $this->db->limit($params['length'], $params['start']);
        }
        $data['result'] = $this->db->get($this->table);

        return $data;
    }

    public function get_all()
    {
        return $this->db->get($this->table);
    }

    public function insert($params = array())
    {
        $retval = FALSE;
        if($this->db->insert($this->table,$params)){
            if($this->db->affected_rows() > 0)
            {
                $retval = TRUE;
            }
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }

        return $retval;
    }

    public function update($params = array())
    {
/*        $data[$params['column_name']] = $params["value"];
        $this->db->update($this->table, $data, array('id' => $params['id']));
        $retval = FALSE;
        if($this->db->affected_rows() > 0)
        {
            $retval = TRUE;
        }
        return $retval;*/

        $retval = FALSE;
        if($this->db->update($this->table, $params, array('id' => $params['id']))){
            $retval = TRUE;
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }
        return $retval;

    }

    public function delete($params = array())
    {
        $retval = FALSE;
        if($this->db->delete($this->table, array('id' => $params['id']))){
            if($this->db->affected_rows() > 0)
            {
                $retval = TRUE;
            }
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }
        return $retval;
    }

    public function enable_all()
    {
        $retval = FALSE;
        $this->db->set('enable','1');
        if($this->db->update($this->table)){
            $retval = TRUE;
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }
        return $retval;
    }

    public function disable_all()
    {
        $retval = FALSE;
        $this->db->set('enable','0');
        if($this->db->update($this->table)){
            $retval = TRUE;
        }else{
            $error = $this->db->error();
            $retval = $error["message"];
        }
        return $retval;
    }

    public function get_where($columns = array())
    {
        return $this->db->get_where($this->table, $columns);
    }

}