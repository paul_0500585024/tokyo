/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 5.7.29-0ubuntu0.18.04.1 : Database - crypto_trading
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`crypto_trading` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `crypto_trading`;

/*Table structure for table `symbol_kucoin` */

DROP TABLE IF EXISTS `symbol_kucoin`;

CREATE TABLE `symbol_kucoin` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `symbol` varchar(45) NOT NULL,
  `base_c` varchar(45) NOT NULL,
  `counter_c` varchar(45) NOT NULL,
  `enable` tinyint(4) DEFAULT NULL,
  `min_diff_trade` decimal(11,5) DEFAULT '1.00000',
  `min_diff_report` decimal(11,5) DEFAULT '0.10000',
  `price` decimal(16,8) DEFAULT NULL,
  `time_range` int(11) DEFAULT NULL,
  `price_diff` decimal(5,2) DEFAULT NULL,
  `price_diff_value` decimal(16,8) DEFAULT NULL,
  `spread_change` decimal(5,2) DEFAULT NULL,
  `spread` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `spread_hedge` decimal(11,5) NOT NULL DEFAULT '0.20000',
  `hedge_market` tinyint(4) NOT NULL DEFAULT '0',
  `max_qty_bid` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `max_qty_ask` decimal(11,5) NOT NULL DEFAULT '0.00000',
  `account` varchar(45) DEFAULT NULL COMMENT 'Kucoin account name ',
  `total_order` int(11) DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=225 DEFAULT CHARSET=latin1;

/*Data for the table `symbol_kucoin` */

insert  into `symbol_kucoin`(`id`,`symbol`,`base_c`,`counter_c`,`enable`,`min_diff_trade`,`min_diff_report`,`price`,`time_range`,`price_diff`,`price_diff_value`,`spread_change`,`spread`,`spread_hedge`,`hedge_market`,`max_qty_bid`,`max_qty_ask`,`account`,`total_order`) values (1,'BNB-BTC','BNB','BTC',0,'1.00000','0.10000','22.29020000',0,'0.10','-0.17331799','0.09','0.10000','0.08000',0,'6.47519','61.85633','kucoin_merry_snap',10),(2,'XMR-BTC','XMR','BTC',0,'1.00000','0.10000','77.53000000',0,'0.50','0.06453278','0.09','0.10000','0.08000',0,'0.00000','9.29568','kucoin_merry_snap',10),(3,'TRX-USDT','TRX','USDT',0,'1.00000','0.10000','0.02005000',0,'0.50','0.40060090','0.11','0.10000','0.08000',0,'39580.93714','31052.85922','kucoin_merry_snap',10),(4,'XMR-ETH','XMR','ETH',0,'1.00000','0.10000','77.53000000',0,'0.50','0.06453278','0.09','0.10000','0.08000',0,'0.00000','9.29568','kucoin_merry_snap',10),(5,'BNB-USDT','BNB','USDT',0,'1.00000','0.00029','22.32890000',0,'0.01','0.08336956','0.09','0.10000','0.08000',0,'6.47519','61.85633','kucoin_merry_snap',10),(6,'TRX-BTC','TRX','BTC',0,'0.23000','0.20000','0.02005000',0,'0.01','0.40060090','0.09','0.10000','0.08000',0,'39580.93714','31052.85922','kucoin_merry_snap',10),(7,'ADA-USDT','ADA','USDT',0,'0.10000','0.20000','0.05899000',0,'0.05','-0.20301133','0.09','0.10000','0.08000',0,'153.00120','26774.29136','kucoin_merry_snap',10),(8,'ADA-BTC','ADA','BTC',0,'0.21000','0.20000','0.05903000',0,'0.01','0.06780810','0.09','0.10000','0.08000',0,'153.00120','26774.29136','kucoin_merry_snap',10),(9,'DASH-BTC','DASH','BTC',0,'0.22000','0.22000','105.74000000',0,'0.01','-0.07560008','0.09','0.10000','0.08000',0,'5.65091','1.66662','kucoin_merry_snap',10),(10,'DASH-USDT','DASH','USDT',0,'1.00000','0.10000','105.67000000',0,'0.01','0.21813354','0.09','0.10000','0.08000',0,'5.65091','1.66662','kucoin_merry_snap',10),(11,'TOMO-BTC','TOMO','BTC',0,'0.20000','0.20000','0.59410000',0,'0.01','0.10109520','0.01','0.10000','0.08000',0,'0.00000','0.00000','kucoin_merry_snap',10),(210,'TOMO-USDT','TOMO','USDT',0,'0.20000','0.20000','0.59410000',0,'0.01','0.10109520','0.09','0.10000','0.08000',0,'1716.75624','1204.82298','kucoin_merry_snap',10),(213,'XRP-BTC','XRP','BTC',0,'1.00000','0.10000','0.27431000',0,'0.50','0.14603337','0.09','0.10000','0.08000',0,'23.71730','119.93333','kucoin_merry_snap',10),(214,'XRP-USDT','XRP','USDT',0,'1.00000','0.10000','0.27431000',0,'0.50','0.14603337','0.09','0.10000','0.08000',0,'23.71730','119.93333','kucoin_merry_snap',10),(215,'ONT-USDT','ONT','USDT',0,'1.00000','0.10000','0.84660000',0,'0.01','-0.10619469','0.01','0.10000','0.08000',0,'11.42046','4.14105','kucoin_merry_snap',10),(216,'ZIL-BTC','ZIL','BTC',0,'1.00000','0.10000','0.00737000',0,'0.50','0.00000000','0.09','0.10000','0.08000',0,'1426.51000','1494.16645','kucoin_merry_snap',10),(217,'ZIL-ETH','ZIL','ETH',0,'1.00000','0.10000','0.00745000',0,'0.50','0.13440860','0.09','0.10000','0.08000',0,'1426.51000','1494.16645','kucoin_merry_snap',10),(218,'ETH-BTC','ETH','BTC',0,'1.00000','0.10000','260.36000000',0,'0.01','0.01152384','0.01','0.10000','0.08000',0,'0.00965','1.03389','kucoin_merry_snap',10),(219,'EOS-USDT','EOS','USDT',0,'1.00000','0.10000','4.06500000',0,'0.01','0.48450091','0.01','0.10000','0.08000',0,'137.94000','0.00000','kucoin_merry_snap',10),(220,'EOS-BTC','EOS','BTC',0,'1.00000','0.10000','4.05360000',0,'0.01','0.20269936','0.01','0.10000','0.08000',0,'137.94000','0.00000','kucoin_merry_snap',10),(221,'NEO-BTC','NEO','BTC',0,'1.00000','0.10000','14.10200000',0,'0.01','0.36296349','0.01','0.10000','0.08000',0,'64.26602','0.03340','kucoin_merry_snap',10),(222,'LTC-BTC','LTC','BTC',0,'1.00000','0.10000','72.15000000',0,'0.01','0.06934813','0.01','0.10000','0.08000',0,'0.00677','0.36729','kucoin_merry_snap',10),(223,'LTC-USDT','LTC','USDT',0,'1.00000','0.10000','72.10000000',0,'0.01','0.09718173','0.01','0.10000','0.08000',0,'0.00677','0.36729','kucoin_merry_snap',10),(224,'ETH-USDT','ETH','USDT',0,'1.00000','0.10000','260.36000000',0,'0.01','0.06918287','0.01','0.10000','0.08000',0,'0.00965','1.03389','kucoin_merry_snap',10);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
